FROM node:lts
ARG WORKSPACE="/home/node/workspace"
ENV WORKSPACE=${WORKSPACE}
USER node
COPY --chown=node:node ./ ${WORKSPACE}
WORKDIR ${WORKSPACE}
RUN npm install
CMD [ "node", "index.js" ]