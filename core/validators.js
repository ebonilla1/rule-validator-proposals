import { Validator, ValidatorResult, SchemaError } from "jsonschema"

function buildResult(instance, schema, options, ctx) {
    var result = new ValidatorResult(instance, schema, options, ctx);
    return result
}

function validateContains(instance, schema, options, ctx) {
    var result = buildResult(instance, schema, options, ctx);
    if (typeof instance !== 'string') return;
    if (typeof schema.contains !== 'string') throw new SchemaError('"contains" expects a string', schema);
    if (instance.indexOf(schema.contains) < 0) {
        result.addError('does not contain the string ' + JSON.stringify(schema.contains))
    }
    return result
}

function includedIn(instance, schema, options, ctx) {
    var result = buildResult(instance, schema, options, ctx);
    if (typeof instance !== 'string') return;
    if (!schema.includedIn instanceof Array) throw new SchemaError('"includedIn" expects a array', schema);
    if (schema.includedIn.indexOf(instance) < 0) {
        result.addError('is not included in ' + JSON.stringify(schema.includedIn))
    }
    return result
}

function build() {
    var validator = registerKeywords(new Validator());

    return validator
}

function registerKeywords(validator) {
    validator.attributes.contains = validateContains
    validator.attributes.includedIn = includedIn
    return validator
}

export default {
    build
}