export default class Result {
    constructor(input) {
        this.input = input;
        this.errors = [];
    }
    isSuccess() {
        return this.errors.length == 0;
    }
    addError(error) {
        this.errors.push(error);
    }
}
