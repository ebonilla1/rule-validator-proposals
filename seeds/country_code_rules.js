export default {
    data: [
        { 
            type: "string",
            required: true
        },
        { 
            type: 'string', 
            minLength: 2, 
            maxLength: 3 
        },
        { 
            type: "string",
            includedIn: ["SV", "MX", "CR"] 
        },
    ]
}