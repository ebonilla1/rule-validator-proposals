import validators from "./core/validators.js"
import countryCodeSeed from "./seeds/country_code_rules.js"
import Result from "./core/result.js"
import ErrorMsg from "./core/error.js"

function main() {
    const input = "SV123456"
    const result = countryCodeSeed.data.reduce((result, schema)=> {
        const validator = validators.build()
        const validatorResult = validator.validate(result.input, schema)
        if (validatorResult.errors.length > 0) {
            for (const err of validatorResult.errors) {
                result.addError(new ErrorMsg(err.message))
            }
        }
        
        return result
    }, new Result(input))

    if (result.isSuccess()) {
        console.log("Input successfully passed all the rules \n")
    } else {
        console.log("Input has errors \n", result.errors)
    }
    
}


main()