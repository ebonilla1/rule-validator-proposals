# Validation

Rules validations, using [JSON Schema](http://json-schema.org/)

## Example

```js
const input = "SV" // country code
const schema = { 
    type: "string",
    includedIn: ["SV", "MX", "CR"] 
}
const validator = validators.build()
const validatorResult = validator.validate(input, schema)
console.log("Errors =>", validatorResult.errors)
```
## Run docker image
In order to run the image, execute the next command

```
docker-compose build
docker-compose run  --rm validator
```

